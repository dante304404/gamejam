﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{
	public void ButtonStart()
    {
        SceneManager.LoadScene("SplashScreen");
    }

    public void ButtonCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void ButtonQuit()
    {
        Application.Quit();
    }

}
