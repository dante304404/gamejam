﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour 
{
	public float heroSpeed;
	public KeyCode left;
	public KeyCode right;
	public KeyCode action;


	void Update () 
	{
		if (Input.GetKey(left)) 
		{
			transform.position += Vector3.right * -heroSpeed * Time.deltaTime;
		}
		if (Input.GetKey(right)) 
		{
			transform.position += Vector3.right * heroSpeed * Time.deltaTime;
		}
	}
}
