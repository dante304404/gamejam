﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ObjectAction : MonoBehaviour 
{
	public bool isActive;
	public GameObject dialogBox;
	public string setText;
	public KeyCode action;
	public KeyCode interact;

	void Update()
	{
		if (isActive && Input.GetKey(action)) 
		{
			Debug.Log ("Postać przeszła przez drzwi. Rozpoczęcia ładowania następnego poziomu.");
			SceneManager.LoadScene ("Menu");
		}
	}


	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			Debug.Log ("Postać ma kontakt z drzwiami");
			isActive = true;
			dialogBox.SetActive(true);
			SetText (dialogBox.GetComponentInChildren<Text> ());
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject.tag == "Player") 
		{
			Debug.Log ("Postać traci kontakt z drzwiami");
			isActive = false;
			dialogBox.SetActive(false);
		}
	}

	void SetText(Text textToSet)
	{
		textToSet.text = setText;
	}

}
